// --- Hamburger Menu Start

const hamburgerMenu = document.querySelector(".hamburger-menu");
const navHamburger = document.querySelector(".nav-list")
const openMenu = document.querySelector(".open-icon")
const closeMenu = document.querySelector(".close-icon")

hamburgerMenu.addEventListener('click', function () {
  openMenu.classList.toggle('active')
  navHamburger.classList.toggle('active')
  closeMenu.classList.remove('active')
});

closeMenu.addEventListener('click', function () {
  closeMenu.classList.toggle('active')
  navHamburger.classList.remove('active')
  openMenu.classList.toggle('active')
});

// --- Hamburger Menu End


const backToTop = document.getElementById('scroll-to-top');

backToTop.addEventListener('click', function (e) {
  scrollTop(0, 1500)
})

function scrollTop(scroll, duration) {
  let doc = document.documentElement;
  let currentTime = duration;
  let speed = 10;

  let animate = () => {
    if (currentTime < 0) return;
    setTimeout(() => {
      doc.scrollTop -= doc.scrollTop / (currentTime / speed)
      console.log(doc.scrollTop)
      currentTime -= speed;
      animate()
    }, speed);
  }

  animate();
}

window.addEventListener('scroll', function (e) {
  if (document.documentElement.scrollTop > 250) {
    backToTop.style.display = 'flex';
  } else {
    backToTop.style.display = 'none'
  }
});


const likeHeart = document.querySelector('.like-icon');
const filledHeart = document.querySelectorAll('.h-filled');
const hollowHeart = document.querySelectorAll('.h-hollow');

likeHeart.addEventListener('click', function () {

  filledHeart.classList.add('active');
  hollowHeart.classList.add('active');
})